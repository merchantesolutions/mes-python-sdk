'''
See a MeS certification manager, or email meS_certification@merchante-solutions.com for a testing profileid & key
Created on Dec 24, 2012
@author: brice
'''

from gateway.Gateway import GatewayRequest, TransactionType, GatewayUrl

g = GatewayRequest(GatewayUrl.Cert, TransactionType.Sale)
g.addCredentials('profile_id_here', 'profile_key_here')
g.addCardData('4012888812348882', '1216')
g.addAmount('1.00')
saleResp = g.run()

print('Sale error code: '+saleResp.getErrorCode())
print('Sale resp text: '+saleResp.getRespText())